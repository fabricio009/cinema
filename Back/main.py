from flask import Flask
from flask_restful import Api
from servicios.UsuarioService import UsuarioService


app = Flask(__name__)
api = Api(app)

api.add_resource(UsuarioService, '/usuarios')

if __name__ == '__main__':
     app.run(port='5000')